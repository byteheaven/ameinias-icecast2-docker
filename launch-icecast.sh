#!/bin/bash

exec docker run \
    --name ameinias-icecast2 \
    --rm -it \
    --volumes-from ameinias \
    -p 8000:8000 \
    tekhedd/ameinias-icecast2 \
    "$@"

